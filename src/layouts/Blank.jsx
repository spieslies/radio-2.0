import React from 'react'

import {AppWrapper} from '../components'
import {NextSeo} from 'next-seo'

export default class Blanc extends React.Component {
  render() {
    const {children, seoConfig} = this.props
    return (
      <AppWrapper>
        <NextSeo {...seoConfig} />
        {children}
      </AppWrapper>
    )
  }
}
