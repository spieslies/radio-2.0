import React from 'react'
import styled from 'styled-components'

import {AppWrapper} from '../components'
import Content from '../components/common/Content'
import {vars} from '../styles/vars'

const ErrorContent = styled(Content)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100vh;
`

const Title = styled.h1`
  text-align: center;
`

const Code = styled.span`
  font-size: 5rem;
`

const Links = styled.div``

const Link = styled.a`
  color: inherit;
  text-decoration: none;
  text-transform: uppercase;
  border-bottom: 1px solid ${vars.colors.underline};
  &:not(:last-child) {
    margin-right: 1rem;
  }
  &:hover {
    color: ${vars.colors.secondary};
    border-bottom-color: ${vars.colors.secondary};
  }
`

export default class Error extends React.Component {
  render() {
    const {code, message} = this.props
    return (
      <AppWrapper>
        <ErrorContent>
          <Title>
            <Code>{code}</Code>
            <br />
            {message}
          </Title>
          <Links>
            <Link href="/">Go to main page</Link>
          </Links>
        </ErrorContent>
      </AppWrapper>
    )
  }
}
