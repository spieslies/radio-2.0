import React from 'react'

import {AppWrapper, Header, Mainer, Footer} from '../components'

export default class Default extends React.Component {
  render() {
    const {children} = this.props
    return (
      <AppWrapper>
        <Header />
        <Mainer>{children}</Mainer>
      </AppWrapper>
    )
  }
}
