const vars = {
  headerHeight: 100,
  colors: {
    white: '#fff',
    light: {
      primary: '#0B0500',
      secondary: '#6F00FF',
      background: '#fff',
    },
    dark: {
      primary: '#fff',
      secondary: '#e63946',
      background: '#0B0500',
    },
  },
  media: {
    w320: '@media (min-width: 320px)',
    w375: '@media (min-width: 375px)',
    w570: '@media (min-width: 570px)',
    w600: '@media (min-width: 600px)',
    w768: '@media (min-width: 768px)',
    w980: '@media (min-width: 980px)',
    w1024: '@media (min-width: 1024px)',
    w1180: '@media (min-width: 1180px)',
    w1280: '@media (min-width: 1280px)',
    w1330: '@media (min-width: 1330px)',
    w1440: '@media (min-width: 1440px)',
    w1760: '@media (min-width: 1760px)',
    w1920: '@media (min-width: 1920px)',
  },
}

export {vars}
