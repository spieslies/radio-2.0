import {vars} from './vars'

const lightTheme = {
  colors: {
    primary: vars.colors.light.primary,
    secondary: vars.colors.light.secondary,
    background: vars.colors.light.background,
  },
}

const darkTheme = {
  colors: {
    primary: vars.colors.dark.primary,
    secondary: vars.colors.dark.secondary,
    background: vars.colors.dark.background,
  },
}

export {lightTheme, darkTheme}
