import {css} from 'styled-components'

const fontFace = css`
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-Black.ttf');
    font-weight: 900;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-ExtraBold.ttf');
    font-weight: 800;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-Bold.ttf');
    font-weight: 700;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-SemiBold.ttf');
    font-weight: 600;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-Medium.ttf');
    font-weight: 500;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-Regular.ttf');
    font-weight: 400;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-Light.ttf');
    font-weight: 300;
    font-style: normal;
  }
  @font-face {
    font-family: 'Unbounded';
    src: url('/static/fonts/Unbounded-ExtraLight.ttf');
    font-weight: 200;
    font-style: normal;
  }
`

export default fontFace
