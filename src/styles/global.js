import {createGlobalStyle} from 'styled-components'
import fontFace from './fontFace'

const GlobalStyles = createGlobalStyle`
    ${fontFace}
    * {
        box-sizing: border-box;
    }
    body {
        font-family: "Unbounded", sans-serif;
        -webkit-font-smoothing: antialiased;
        background: ${(props) => props.theme.colors.background};
        color: ${(props) => props.theme.colors.primary};
        &::-webkit-scrollbar {
            width: .3em;
        }
        &::-webkit-scrollbar-track {
            box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.1);
        }
        &::-webkit-scrollbar-thumb {
        background-color: ${(props) => props.theme.colors.primary};
    }
        @media screen and (min-width: 900px) {
            font-size: 1em;
        }
    }
    ::selection {
        color: ${(props) => props.theme.colors.background};
        background: ${(props) => props.theme.colors.primary};
    }
`

export default GlobalStyles
