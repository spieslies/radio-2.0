import {format, isAfter, isBefore, isToday} from 'date-fns'
import ru from 'date-fns/locale/ru'
export const formatDateTitle = (date, toBeAnnounced) => {
  const today = new Date()
  const eventDate = new Date(date)

  if (toBeAnnounced) {
    return null
  }

  if (isToday(eventDate)) {
    if (isAfter(eventDate, today)) {
      return `Начало в ${format(eventDate, 'HH:mm', {locale: ru})}`
    }
    return `Сейчас в эфире:`
  } else if (isBefore(eventDate, today)) {
    return format(eventDate, 'd MMMM yy', {locale: ru})
  }

  return format(eventDate, 'd MMMM в HH:mm', {locale: ru})
}
