import React from 'react'

const StopIcon = () => {
  return (
    <svg height="48" width="48" xmlns="http://www.w3.org/2000/svg">
      <path d="M0 0h48v48H0z" fill="none"/>
      <path d="M12 12h24v24H12z"/>
    </svg>
  )
}

export default StopIcon
