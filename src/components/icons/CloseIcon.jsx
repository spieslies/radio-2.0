import React from 'react'

const CloseIcon = () => {
  return (
    <svg height="48" width="48" xmlns="http://www.w3.org/2000/svg">
      <path d="M47.998 4.247L43.758.002 24.001 19.758 4.245.002.004 4.247l19.754 19.754L.004 43.755l4.246 4.24 19.751-19.751 19.751 19.751 4.246-4.24-19.754-19.754z"/>
    </svg>
  )
}

export default CloseIcon

