import React from 'react'
import {withRouter} from 'next/router'
import styled, {keyframes} from 'styled-components'

import {vars} from '../../styles/vars'
import {formatDateTitle} from '../../utils'
import {isBefore, isToday} from 'date-fns'

const blink = keyframes`
  0% {opacity: 1;}
  50% {opacity: 0;}
  100% {opacity: 1}
`

const SEvent = styled.a`
  position: relative;
  color: inherit;
  font-size: 1rem;
  font-weight: 300;
  display: flex;
  flex-direction: column;
  border-bottom: 0.1rem solid transparent;
  transition: opacity 133ms ease-in-out;
  &:hover {
    opacity: 0.5;
  }
  ${vars.media.w980} {
    display: inline;
    margin-bottom: 1rem;
    font-size: 1.2rem;
    &:hover {
      cursor: pointer;
      color: inherit;
      opacity: 1;
      border-bottom-color: white;
    }
  }
`

const Name = styled.span`
  font-weight: 600;
  margin-bottom: 0.2rem;
  ${vars.media.w980} {
    margin-bottom: 0;
    margin-right: 1rem;
  }
`

const DateTitle = styled.span`
  display: inline-block;
  position: relative;
  opacity: 0.4;
  margin-bottom: 0.2rem;
  &:before {
    content: '';
    position: absolute;
    display: ${(props) => (props.isLiveNow ? 'block' : 'none')};
    top: 45%;
    left: -1.2em;
    width: 0.7em;
    height: 0.7em;
    background: ${(props) => props.theme.colors.secondary};
    border-radius: 100%;
    transform: translateY(-50%);
    filter: blur(0.1rem);
    animation: ${blink} 5s ease-in-out infinite;
  }
  ${vars.media.w980} {
    margin-bottom: 0;
    margin-right: 1rem;
    ${SEvent}:hover & {
      opacity: 1;
    }
  }
`

const Description = styled.span`
  width: 100%;
  opacity: 0.4;
  ${vars.media.w980} {
    ${SEvent}:hover & {
      opacity: 1;
    }
  }
`
const Wrapper = styled.div`
  margin-bottom: 1.5rem;
  &:last-child {
    margin-bottom: 0;
  }
`

class Event extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showPlayer: false,
    }
  }

  handleClick() {
    const {router, event} = this.props
    router.push(`/event/${event.id}`)
  }

  render() {
    const {
      event: {date, description, name, toBeAnnounced},
    } = this.props

    const today = new Date()
    const eventDate = new Date(date)
    const formattedDate = formatDateTitle(date, toBeAnnounced)
    const isLiveNow = isToday(eventDate) && isBefore(eventDate, today)

    return (
      <Wrapper>
        <SEvent onClick={() => this.handleClick()}>
          <Name>{name}</Name>
          {formattedDate && (
            <DateTitle isLiveNow={isLiveNow}>{formattedDate}</DateTitle>
          )}
          {description && <Description>{description}</Description>}
        </SEvent>
      </Wrapper>
    )
  }
}

export default withRouter(Event)
