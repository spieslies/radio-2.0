import React from 'react'
import styled, {keyframes} from 'styled-components'
import Content from '../common/Content'
import {CloseIcon} from '../icons'
import {formatDateTitle} from '../../utils'
import {vars} from '../../styles/vars'
import EventCountdown from '../countdown/EventCountdown'

const gradientAnimation = keyframes`
  0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
`

const SEventModal = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background: black;
  z-index: 1000;
`

const EventModalContent = styled(Content)`
  position: relative;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 2rem;
  ${vars.media.w1180} {
    flex-direction: row;
  }
`

const Title = styled.h1`
  margin: 0.5rem 0;
  line-height: 1;
  font-size: 1.2rem;
  color: white;
  pointer-events: none;
  ${vars.media.w1180} {
    font-size: 2.5rem;
  }
`

const EventModalInfo = styled.div`
  width: 100%;
  ${vars.media.w1180} {
    width: auto;
  }
`

const CloseBtn = styled.button`
  position: absolute;
  top: 1em;
  right: 1em;
  padding: 0;
  border: 0;
  background: none;
  color: white;
  font-size: 2em;
  font-weight: bold;
  transition: 133ms ease-in-out;
  outline: none;
  opacity: 0.2;
  & svg {
    fill: white;
  }
  &:hover {
    cursor: pointer;
    opacity: 1;
  }

  &:active {
    border-top: none;
    border-right: none;
    border-left: none;
  }
`
const VideoContainer = styled.iframe`
  width: 100%;
  max-width: 48rem;
  height: 12rem;
  background: linear-gradient(-45deg, #1d1d1d, #202020, #424242, #272727);
  background-size: 400% 400%;
  animation: ${gradientAnimation} 4s ease infinite;
  ${vars.media.w768} {
    height: 22rem;
  }
  ${vars.media.w1180} {
    height: 32rem;
  }
`
const Agenda = styled.div`
  text-align: center;
  line-height: 1.4;
`

const EventModal = ({event, onClose}) => {
  const {date, toBeAnnounced, liveVideoLink, name, description, cover} = event
  return (
    <SEventModal>
      <EventModalContent>
        <CloseBtn onClick={onClose}>
          <CloseIcon />
        </CloseBtn>
        {liveVideoLink && (
          <VideoContainer
            src={event.liveVideoLink}
            frameBorder="0"
            allowFullScreen
          />
        )}

        {liveVideoLink ? (
          <EventModalInfo>
            <span>{formatDateTitle(date, toBeAnnounced)}</span>
            <Title>{name}</Title>
            <span>{description}</span>
          </EventModalInfo>
        ) : (
          <Title>
            {toBeAnnounced ? (
              <Agenda>
                Скоро
                <br />
                <span>{name}</span>
              </Agenda>
            ) : (
              <>
                Стрим <EventCountdown date={event.date} />
              </>
            )}
          </Title>
        )}
      </EventModalContent>
    </SEventModal>
  )
}

export default EventModal
