import React from 'react'
import {isAfter, isToday} from 'date-fns'
import styled from 'styled-components'

import Event from './Event'
import {vars} from '../../styles/vars'

const SEventList = styled.ol`
  display: flex;
  flex-direction: column;
  margin: 0;
  padding: 0;
`

const EventSection = styled.section`
  &:not(:last-child) {
    margin: 0 0 4em 0;
  }
`

const EventsWrapper = styled.div`
  display: block;
`

const EventSectionTitle = styled.h2`
  font-weight: 400;
  margin: 0 0 1em 0;
  opacity: 0.4;
`

const EventList = ({data}) => {
  const today = new Date()

  const upcomingEvents = data
    .filter(
      (event) =>
        isAfter(new Date(event.date), today) && !isToday(new Date(event.date))
    )
    .sort((a, b) => new Date(b.date) - new Date(a.date))

  const currentEvents = data
    .filter((event) => isToday(new Date(event.date)))
    .sort((a, b) => new Date(a.date) - new Date(b.date))

  const pastEvents = data
    .filter(
      (event) =>
        !isAfter(new Date(event.date), today) && !isToday(new Date(event.date))
    )
    .sort((a, b) => new Date(b.date) - new Date(a.date))

  const hasUpcomingEvents = upcomingEvents.length !== 0
  const hasCurrentEvent = currentEvents.length !== 0
  const hasPastEvents = pastEvents.length !== 0

  return (
    <SEventList>
      {hasUpcomingEvents && (
        <EventSection>
          <EventSectionTitle>Скоро</EventSectionTitle>
          <EventsWrapper>
            {upcomingEvents.map((event, key) => (
              <Event key={key} event={event} />
            ))}
          </EventsWrapper>
        </EventSection>
      )}
      {hasCurrentEvent && (
        <>
          <EventSection>
            <EventSectionTitle>Сегодня</EventSectionTitle>
            <EventsWrapper>
              {currentEvents.map((event, key) => (
                <Event key={key} event={event} />
              ))}
            </EventsWrapper>
          </EventSection>
        </>
      )}
      {hasPastEvents && (
        <EventSection>
          <EventSectionTitle>Архив</EventSectionTitle>
          <EventsWrapper>
            {pastEvents.map((event, key) => (
              <Event key={key} event={event} />
            ))}
          </EventsWrapper>
        </EventSection>
      )}
    </SEventList>
  )
}

export default EventList
