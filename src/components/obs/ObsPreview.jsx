import React from 'react'
import styled from 'styled-components'
import EventCountdown from '../countdown/EventCountdown'
import {BASE_URL} from '../../consts'
const SObsPreview = styled.div`
  display: flex;
  align-items: center;
  padding: 20px 50px;
  flex: 1;
  background-image: url(${BASE_URL}${(props) => props.bgImage});
  background-size: cover;
  background-position: center;
  color: #fff;
`

const Title = styled.h1`
  font-size: 60px;
  & span {
    position: relative;
    padding: 0.5rem 1rem;
    background: #000;
    &:before {
      content: '';
      background: #000;
      width: 10px;
      height: 100%;
      position: absolute;
      left: -10px;
      top: 0;
      bottom: 0;
      display: block;
    }
    &:after {
      content: '';
      background: #000;
      width: 10px;
      height: 100%;
      position: absolute;
      top: 0;
      bottom: 0;
      right: -10px;
      display: block;
    }
  }
`

class ObsPreview extends React.Component {
  render() {
    const {event} = this.props
    return (
      <SObsPreview bgImage={event.cover?.url}>
        <Title>
          <span>{event.name}</span>
          <br />
          <EventCountdown date={event.date} />
        </Title>
      </SObsPreview>
    )
  }
}

export default ObsPreview
