import React from 'react'
import Countdown from 'react-countdown'
import plural from 'plural-ru'
import styled from 'styled-components'

const Time = styled.div`
  display: inline-block;
  width: 365px;
  background: #000;
`

const formatTime = (value) => (value < 10 ? `0${value}` : value)

const CountdownContent = ({days, hours, minutes, seconds, completed}) => {
  if (completed) {
    return <span>скоро начнется</span>
  } else if (days > 0) {
    const a = 20
    return (
      <span>
        начнется через &nbsp;{days}&nbsp;{plural(days, 'день', 'дня', 'дней')}
      </span>
    )
  } else {
    return (
      <span>
        начнется через{' '}
        <Time>
          {formatTime(hours)}:{formatTime(minutes)}:{formatTime(seconds)}
        </Time>
      </span>
    )
  }
}

class EventCountdown extends React.Component {
  render() {
    return <Countdown date={this.props.date} renderer={CountdownContent} />
  }
}

export default EventCountdown
