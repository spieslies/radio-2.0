import React from 'react'
import styled from 'styled-components'

const SContent = styled.div`
  width: 100%;
  margin: 0 auto;
  padding: 0 2em;
  @media screen and (min-width: 900px) {
    padding: 0 7em;
  }
`

const Content = ({children, className}) => {
  return <SContent className={className}>{children}</SContent>
}

export default Content
