import React from 'react'
import {useRouter} from 'next/router'
import classnames from 'classnames'
import styled, {keyframes} from 'styled-components'

import Content from './Content'
import {vars} from '../../styles/vars'

const morpheus = keyframes`
  0% { border-radius: 30% 70% 70% 30% / 30% 30% 70% 70%; }
	20% { border-radius: 60% 40% 40% 60% / 60% 25% 75% 40%; }
	40% { border-radius: 70% 30% 65% 35% / 80% 45% 55% 20%; }
	60% { border-radius: 30% 70% 70% 30% / 20% 30% 70% 80%; }
	80% { border-radius: 40% 60% 40% 60% / 70% 55% 45% 30%; }
	100% { border-radius: 30% 70% 70% 30% / 30% 30% 70% 70%; }

`

const SHeader = styled.div`
  position: fixed;
  width: 100%;
  height: ${vars.headerHeight}px;
  z-index: 999;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const HeaderContent = styled(Content)`
  display: flex;
  align-items: center;
  flex-direction: column;
  ${vars.media.w570} {
    flex-direction: row;
  }
`

const Logo = styled.div`
  position: relative;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  text-align: center;
  background-clip: text;
  overflow: hidden;
  padding: 0.3em;
  border: 0.2em solid ${(props) => props.theme.colors.primary};
  &:hover {
    cursor: default;
  }
`

const Text = styled.span`
  font-weight: 700;
  z-index: 1;
  text-transform: uppercase;
`

const Links = styled.div`
  margin: 1rem 0 0 0;
  z-index: 1;
  ${vars.media.w570} {
    margin: 0 0 0 auto;
  }
`

const HeaderLink = styled.a`
  color: inherit;
  text-decoration: none;
  font-weight: 900;
  &:not(:last-child) {
    margin-right: 1rem;
  }
  &:hover,
  &.active {
    text-decoration-line: underline;
    text-decoration-thickness: 2px;
  }
`

const Blob = styled.div`
  width: 100px;
  height: 100px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-image: linear-gradient(45deg, #3023ae 0%, #ff0099 100%);
  border-radius: 100%;
  animation: ${morpheus} 10s linear infinite;
`

const Blob1 = styled(Blob)`
  top: 50%;
  left: 10%;
  background-image: linear-gradient(45deg, #a35c19 0%, #ff0099 100%);
  animation: ${morpheus} 20s linear infinite;
`
const Blob2 = styled(Blob)`
  top: 50%;
  left: 80%;
  background-image: linear-gradient(45deg, #ff07d6 0%, #0aee62 100%);
  animation: ${morpheus} 10s linear infinite;
`

const MainNav = styled.span`
  margin: 1rem 0 0 0;
  ${vars.media.w570} {
    margin: 0 0 0 2rem;
  }
`

const Header = () => {
  const router = useRouter()
  console.log(router.pathname)
  return (
    <SHeader>
      <HeaderContent>
        <Logo>
          {/* <Blob1 />
          <Blob2 />
          <Blob /> */}
          <Text>радио410</Text>
        </Logo>
        <MainNav>
          {/* <HeaderLink href="/">home</HeaderLink> */}
          {/* <HeaderLink
            href="/audio"
            className={classnames({active: router.pathname === '/audio'})}
          >
            audio
          </HeaderLink> */}
        </MainNav>
        <Links>
          <HeaderLink href="https://vk.com/radio410" target="_blank">
            вк
          </HeaderLink>
          <HeaderLink href="https://t.me/r4d10com" target="_blank">
            тг
          </HeaderLink>
          {/* <HeaderLink href="mailto:hello@r4d10.com" target="_blank">
            hello@r4d10.com
          </HeaderLink> */}
          {/* <HeaderLink
            href="https://r4d10.com/donate"
            target="_blank"
            rel="nofollow"
          >
            donate
          </HeaderLink> */}
        </Links>
      </HeaderContent>
    </SHeader>
  )
}

export default Header
