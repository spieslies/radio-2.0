import React from 'react'
import styled from 'styled-components'

const SAppWrapper = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
`

const AppWrapper = ({children}) => {
  return <SAppWrapper>{children}</SAppWrapper>
}

export default AppWrapper
