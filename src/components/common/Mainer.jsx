import React from 'react'
import styled from 'styled-components'

import Content from './Content'
import {vars} from '../../styles/vars'

const SMainer = styled.div`
  flex: 1;
  padding: ${vars.headerHeight * 1.5}px 0 3em;
`

const Mainer = ({children}) => {
  return (
    <SMainer>
      <Content>{children}</Content>
    </SMainer>
  )
}

export default Mainer
