import React from 'react'
import styled from 'styled-components'
import Content from './Content'

const SFooter = styled.div`
  padding: 3em 0;
`

const MailLink = styled.span`
  color: inherit;
  font-size: .7em;
  text-decoration: none;
  text-transform: uppercase;
`

const Footer = () => {
  return (
    <SFooter>
      <Content>
        <MailLink href="mailto:hello@r4d10.com" target="_blank">
          2020
        </MailLink>
      </Content>
    </SFooter>
  )
}

export default Footer
