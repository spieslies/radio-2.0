module.exports = {
  siteUrl: process.env.SITE_URL || 'https://r4d10.com',
  generateRobotsTxt: true,
}
