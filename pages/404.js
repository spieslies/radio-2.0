import React from 'react'

import {Error} from '../src/layouts'

const Custom404 = () => {
  return <Error code={404} message="this page doesn't exist" />
}

export default Custom404
