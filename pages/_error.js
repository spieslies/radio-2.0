import React from 'react'

import {Error as ErrorLayout} from '../src/layouts'

const Error = ({statusCode}) => {
  return (
    <ErrorLayout
      code={statusCode || ''}
      message={
        statusCode
          ? `Seems like something wrong with our server.`
          : 'An error occurred on client'
      }
    />
  )
}

Error.getInitialProps = ({res, err}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return {statusCode}
}

export default Error
