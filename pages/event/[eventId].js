import {useEffect} from 'react'
import {useRouter} from 'next/router'
import Modal from 'react-modal'

import {EventModal} from '../../src/components'
import {Blank} from '../../src/layouts'
import {format} from 'date-fns'

Modal.setAppElement('#__next')

export async function getServerSideProps(context) {
  const res = await fetch(
    `https://r4d10.com/api/events?published=true&id=${context.params.eventId}`
  )
  const data = await res.json()
  return {
    props: {data},
  }
}

const EventPage = ({data}) => {
  const event = data[0]
  const router = useRouter()

  useEffect(() => {
    router.prefetch('/')
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const closeModal = () => {
    router.push('/')
  }

  const getSeoConfig = (event) => {
    const {name, description, date, id, cover, toBeAnnounced} = event
    const eventUrl = `https://r4d10.com/event/${id}`
    const imageUrl = cover
      ? `https://r4d10.com/api/${cover.url}`
      : 'https://r4d10.com/api/uploads/fallback_img_83c93e3ef0.png'
    const eventDate = format(new Date(date), 'dd/MM/yy')
    const eventTitle = toBeAnnounced
      ? `${name} @ РАДИО 410`
      : `${eventDate} ${name} @ РАДИО 410`
    return {
      title: eventTitle,
      description: description,
      canonical: eventUrl,
      openGraph: {
        url: eventUrl,
        title: eventTitle,
        description: description,
        images: [
          {
            url: imageUrl,
            width: 800,
            height: 600,
            alt: name,
          },
          {url: imageUrl},
        ],
        site_name: 'РАДИО 410',
      },
    }
  }

  return (
    <Blank seoConfig={getSeoConfig(event)}>
      <Modal
        isOpen={true}
        onRequestClose={() => router.push('/')}
        contentLabel="Event modal"
      >
        <EventModal event={event} onClose={closeModal} />
      </Modal>
    </Blank>
  )
}

export default EventPage
