import React, {useState, useRef, useEffect} from 'react'
import fetch from 'node-fetch'
import styled from 'styled-components'

import {Default} from '../src/layouts'
import {BASE_URL} from '../src/consts'
import {PlayIcon, StopIcon} from '../src/components/icons'
import {vars} from '../src/styles/vars'

export async function getServerSideProps() {
  const res = await fetch('https://r4d10.com/api/mixes')
  const data = await res.json()
  return {
    props: {data},
  }
}

const Item = styled.div`
  display: flex;
  flex: 0;
  width: fit-content;
  align-items: center;
  margin-bottom: 3rem;
  & svg {
    flex-shrink: 0;
    fill: ${vars.colors.white};
    opacity: 0.5;
  }
  &:hover {
    cursor: pointer;
    & svg {
      opacity: 1;
    }
  }
`

const Title = styled.div`
  margin-bottom: 0.5rem;
  font-weight: bold;
`

const Tags = styled.span`
  font-size: 0.8rem;
  opacity: 0.5;
`

const Info = styled.div`
  margin-left: 1rem;
`

const Audio = ({data}) => {
  const [currentTrack, setCurrentTrack] = useState({
    id: null,
    url: null,
    title: null,
  })
  const [isPlaying, setIsPlaying] = useState(false)
  const playerRef = useRef(null)

  const getTrackTitle = (item) =>
    item.mix.name.substr(0, item.mix.name.lastIndexOf('.')) || item.mix.name

  const togglePlay = (track) => {
    const {onPlay, onPause, onAbort} = playerRef?.current.props
    setCurrentTrack(track)
    if (isPlaying) {
      if (currentTrack.id !== track.id) {
        onAbort()
        return
      }
      onPause()
      return
    }
    onPlay()
  }

  return (
    <Default>
      {data.map((item) => {
        return (
          <Item
            key={item.id}
            onClick={() =>
              togglePlay({
                id: item.id,
                url: `${BASE_URL}${item.mix.url}`,
                title: getTrackTitle(item),
              })
            }
          >
            {currentTrack.id === item.id && isPlaying ? (
              <StopIcon />
            ) : (
              <PlayIcon />
            )}

            <Info>
              <Title>{getTrackTitle(item)}</Title>
              <Tags>{item.description}</Tags>
            </Info>
          </Item>
        )
      })}
      
    </Default>
  )
}

export default Audio
