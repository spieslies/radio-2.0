import React from 'react'
import fetch from 'node-fetch'

import {Blank} from '../src/layouts'
import {ObsPreview} from "../src/components/obs";

export async function getServerSideProps() {
  const res = await fetch('https://r4d10.com/api/events?obsPreview=true')
  const data = await res.json()
  return {
    props: {data},
  }
}

const Obs = ({data}) => {
  const event = data[0]
  return (
    <Blank>
      <ObsPreview event={event}/>
    </Blank>
  )
}

export default Obs
