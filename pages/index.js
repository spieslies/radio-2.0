import React from 'react'
import fetch from 'node-fetch'

import {Default} from '../src/layouts'
import {EventList} from '../src/components'

export async function getServerSideProps() {
  const res = await fetch('https://r4d10.com/api/events?published=true')
  const data = await res.json()
  return {
    props: {data},
  }
}

const Home = ({data}) => {
  return (
    <Default>
     <EventList data={data}/>
    </Default>
  )
}

export default Home
