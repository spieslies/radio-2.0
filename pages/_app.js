import React, {Fragment} from 'react'
import App from 'next/app'
import Head from 'next/head'
import {Normalize} from 'styled-normalize'
import {ThemeProvider} from 'styled-components'

import {GlobalStyles} from '../src/styles'
import {darkTheme, lightTheme} from '../src/styles/themes'

const prod = process.env.NODE_ENV === 'production'

const getTheme = () => {
  // if (typeof window !== "undefined" && window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
  //   return darkTheme
  // }
  return darkTheme
}

export default class MyApp extends App {
  constructor(props) {
    super(props)
  }
  render() {
    const {Component, pageProps} = this.props
    return (
      <Fragment>
        <Head>
          <title>РАДИО 410</title>
          <meta
            name="keyword"
            content="электронная музыка онлайн, электронная музыка слушать онлайн, электронная музыка онлайн бесплатно, электронная музыка слушать онлайн бесплатно, лучшая электронная музыка онлайн, радио электронной музыки слушать онлайн, электронная музыка без слов онлайн, фанк слушать онлайн, радио онлайн бесплатно, диджей сеты онлайн"
          />
          <meta name="description" content="Прямые трансляции диджей сетов" />
          <meta content="width=device-width, initial-scale=1" name="viewport" />
          <meta name="yandex-verification" content="5214be503cef3145" />
        </Head>
        <ThemeProvider theme={getTheme()}>
          <Normalize />
          <GlobalStyles theme={getTheme()} />
          <Component {...pageProps} />
        </ThemeProvider>

        {prod ? (
          <>
            <div
              /* eslint-disable-next-line react/no-danger */
              dangerouslySetInnerHTML={{
                __html:
                  '<script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");ym(56004118, "init", { clickmap:true,trackLinks:true,accurateTrackBounce:true,webvisor:true});</script><noscript><div><img src="https://mc.yandex.ru/watch/56004118" style="position:absolute; left:-9999px;" alt="" /></div></noscript>',
              }}
            />
            <div
              /* eslint-disable-next-line react/no-danger */
              dangerouslySetInnerHTML={{
                __html:
                  "<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-130567708-2\"></script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js, new Date());gtag('config', 'UA-130567708-2');</script>",
              }}
            />
          </>
        ) : null}
      </Fragment>
    )
  }
}
